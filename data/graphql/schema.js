/**
 * Created by Lukas on 07-Apr-17.
 */
import {merge} from 'lodash';

//Import Types
import BillSchema from './models/Bill/billSchema';
import GroupSchema from './models/Group/groupSchema';
import ItemSchema from './models/Item/itemSchema';
import ItemTypeSchema from './models/ItemType/itemTypeSchema';
import RecipeSchema from './models/Recipe/recipeSchema';
import RecipeItemSchema from './models/RecipeItem/recipeItemSchema';
import TagSchema from './models/Tag/tagSchema';
import UserSchema from './models/User/userSchema';


//Import Resolvers
import UserResolvers from  './models/User/userResolvers';
import BillResolvers from  './models/Bill/billResolvers';
import GroupResolvers from './models/Group/groupResolvers';
import ItemResolvers from './models/Item/itemResolvers';

//Import HelperTypes
import AuthErrorSchema from './helper/AuthError/authErrorSchema';
import AuthResponseSchema from './helper/AuthResponse/authResponseSchema';
import UserActionsSchema from './helper/UserActions/userActionsSchema';


//Merge Types

//, ItemSchema, ItemTypeSchema, RecipeSchema, RecipeItemSchema, TagSchema, UserSchema, AuthErrorSchema, AuthResponseSchema


//import Queries and Mutations
import Queries from './queries';


//Merge Resolvers
const resolvers = merge(BillResolvers(), UserResolvers(), GroupResolvers(), ItemResolvers());

import { makeExecutableSchema } from 'graphql-tools';


export default makeExecutableSchema({
    typeDefs: [UserSchema, Queries, AuthResponseSchema, BillSchema, ItemSchema, ItemTypeSchema, AuthErrorSchema, GroupSchema, UserActionsSchema],
    resolvers: BillResolvers()
});