/**
 * Created by Lukas on 27-Apr-17.
 */
const queries = `    
    type Query {
        users: [User]
        user(username: String, id: String): User
        login(username: String!, password: String!): AuthResponse
        bill(id: String!): Bill
        bills(owner: String, paidBy: String, group: String, fromDate: Int, toDate: Int, consumer: String): [Bill]
        group(name: String!): Group
        groups: [Group]
        managedGroups(username: String!): [Group]
        budget(username: String!, groupname: String!): Float
        getSelf: User
        item(id: String!): Item
        billsByGroup(groupname: String!, from: String, to: String): [Bill]
        groupBudget(group: String): GroupBudget
        groupCategorySummary(group: String!, from: String, to: String): [CategorySum]
        getAllBudgets(username: String!): [UserBudget]
        getUserSummary(username: String!): UserSummary
        
    }    
    type Mutation {
        addUser(input: UserInput!): User
        deleteUser(username: String!): User
        newBill(input: BillInput!): Bill
        addGroup(input: GroupInput!): Group
        updateGroup(input: GroupInput!): Group
        addUserToGroup (username: String!, groupname: String!): User
        addGroupToUser(username: String!, groupname: String!): User
        addCategory(category: String!, group: String!): String
        deleteCategory(category: String!, group: String!): String
        updateUser(username: String!, input: UserInput!): User
        deleteGroup(groupname: String!): Group
        deleteItem(itemId: String!): Item
        deleteBill(id: String!): Bill
        newItem(input: ItemInput!): Item
        changePassword(username: String!, currentPassword: String!, newPassword: String!): String
        addModerator(groupname: String!, username: String!): String
    }
`;

export default () => [queries];