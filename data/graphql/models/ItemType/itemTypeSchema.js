/**
 * Created by Luki on 31.03.17.
 */
import Tag from '../Tag/tagSchema';
import Recipe from '../Recipe/recipeSchema';

const ItemType = `
    type ItemType {
        name: String!
        id: String!
        pricePerUnit: Float
        unit: String
    }
    input ItemTypeInput {
        name: String
        pricePerUnit: Float
        unit: String
    }
`;

//price hier ist Durchschnittspreis der anhand der Einkäufe errechnet werden kann. Angebote vllt noch rausrechnen oder so

export default () => [ItemType];