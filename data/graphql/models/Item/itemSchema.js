/**
 * Created by Luki on 31.03.17.
 */
import ItemType from '../ItemType/itemTypeSchema';
import UserType from '../User/userSchema';

const Item = `
    type Item {
        id: String!
        name: String!
        type: ItemType!
        price: Float
        amount: Int
        unitSize: Float
        consumers: [User]
        date: String
        group: Group
        paidBy: User
        category: String
    }
    input ItemInput {
        name: String
        type: ItemTypeInput
        price: Float
        amount: Int
        unitSize: Float
        consumers: [String]
        date: String
        category: String
    }
`;

export default () => [Item];