/**
 * Created by Lukas on 27-Apr-17.
 */
import * as Item from '../../../businessLogic/repositories/Item'
import * as User from '../../../businessLogic/repositories/User'

const ItemResolvers = {
    Query: {
        item: async(obj, {id}, context) => {
            if (id) {
                return await Item.getItemById(id, context);
            }
        },
    },

    Mutation: {
        newItem: async (obj, {input }, context) => {
            return await Item.newItem(input, context);
        },
        deleteItem: async (obj, {itemId}, context) => {
            return await Item.deleteItemById(itemId, context);
        }
    },
    Item : {
        date: async(item, _, context) => {
            return new Date(item.date).toISOString();
        },
        consumers: async(item, _, context) => {
            return await User.getUsersById(item.consumers, context);
        },
        paidBy: async(item, _, context) => {
            return await User.getUserById(item.paidBy, context);
        }

    }
    };

export default () => ItemResolvers;