/**
 * Created by Lukas on 07-Apr-17.
 */

import ItemType from '../ItemType/itemTypeSchema';

const Recipe = `
    type Recipe {
        name: String!
        description: String
        ingredients: [ItemType]
        members: [User]
    }
`;

export default ()=> [Recipe, ItemType];