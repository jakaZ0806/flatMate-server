/**
 * Created by Luki on 31.03.17.
 */
import Group from "../Group/groupSchema";

const User = `
    type User {
        username: String
        email: String
        password: String
        admin: Boolean
        firstName: String
        lastName: String
        groups: [Group]
        changePassword: Boolean,
        moderator: [Group],
        superUser: Boolean
    }
    
    type UserBudget {
        group: Group
        user: User
        budget: Float
    }
    
    input UserInput {
        username: String
        email: String
        password: String
        admin: Boolean
        firstName: String
        lastName: String
    }

    type UserSummary {
        user: User
        groupBudgets: [GroupBudget]
    }
`;

export default () => [User, Group];