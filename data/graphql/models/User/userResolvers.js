/**
 * Created by Lukas on 11-Apr-17.
 */
import * as User from '../../../businessLogic/repositories/User'
import * as Group from '../../../businessLogic/repositories/Group'
import * as Auth from '../../../businessLogic/auth';
import * as budgetCalculator from '../../../businessLogic/budgetCalculator';

const UserResolvers = {
    Query: {
        users: async (obj, args, context) => {
            return User.getAllUsers(context);
        },

        user: async (obj, {username, id}, context) => {
            if (id) {
                return await User.getUserById(id, context);
            }
            if (username) {
                return await User.getUserByUsername(username, context);
            }
        },
        budget: async (obj, {username, groupname}, context) => {
            const user = await User.getUserByUsername(username, context);
            const group = await Group.getGroupByName(groupname, context);
            return await budgetCalculator.getBudgetForUser(user, group);
        },

        login: async (obj, {username, password}, context) => {
            return await Auth.login(username, password, context);

        },
        getSelf: async(obj, args, context) => {
            return await User.getUserById(context.user._id, context);
        },
        getAllBudgets: async(obj, {username}, context) => {
            return await User.getAllBudgets(username, context);
        },
        getUserSummary: async(obj, {username}, context) => {
            return await User.getUserSummary(username, context);
        }

    },
    Mutation: {
        addUser: async (obj, {input}, context) => {
            return await User.addUser(input, context);
        },

        deleteUser: async (obj, {username}, context) => {
            return await User.deleteUser(username, context);
        },
        updateUser: async (obj, args, context) => {
            return await User.updateUser(args, context);
        },
        addGroupToUser: async (obj, {username, groupname}, context) => {
            await Group.addUserToGroup(username, groupname, context);
            return await User.addUserToGroup(username, groupname, context);
        },
        changePassword: async (obj, {username, currentPassword, newPassword}, context) => {
            return await Auth.changePassword(username, currentPassword, newPassword, context);
            
        }
    },

    User: {
        groups: async (user, _,context) => {
            return await Group.getGroupsById(user.groups, context);
        },
        moderator: async (user, _, context) => {
            return await Group.getGroupsById(user.moderator, context);
        },
        password: async (user, _, context) => {
            return ''; /*never return password*/
        },
    }
};

export default () => UserResolvers;