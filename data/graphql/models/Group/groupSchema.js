/**
 * Created by Luki on 31.03.17.
 */
import User from '../User/userSchema';

const Group = `
    type Group {
        name: String!
        description: String
        id: String!
        members: [User],
        categories: [String],
        moderators: [User],
        archived: Boolean
    }
    type GroupBudget {
        group: Group
        ownBudget: UserBudget
        budgets: [UserBudget]
    }
    type CategorySum {
        name: String
        value: Float
    }
    input GroupInput {
        name: String
        description: String
        initialMember: String
     }
`;

export default ()=> [Group, User];