/**
 * Created by Lukas on 28-Apr-17.
 */
import * as Group from '../../../businessLogic/repositories/Group'
import * as User from '../../../businessLogic/repositories/User'

const GroupResolvers = {
    Query: {
        group: async (obj, {name}, context) => {
            if (name) {
                return await Group.getGroupByName(name, context);
            }
        },
        groups: async (obj, args, context) => {
            return await Group.getAllGroups(context);
        },
        managedGroups: async (obj, {username}, context) => {
            return await Group.getManagedGroups(username, context);
        },
        groupBudget: async(obj, {group}, context) => {
            return await Group.getGroupBudget(group, context);
        },
        groupCategorySummary: async(obj, {group, from, to}, context) => {
            if (!from) {
                from = new Date(0);
            }
            if (!to) {
                to = new Date(Date.now());
            }
            return await Group.getCategorySummary(group, from, to, context);
        }
    },
    Mutation: {
        addGroup: async (obj, args, context) => {
            return await Group.newGroup(args.input, context);
        },
        updateGroup: async (obj, args, context) => {
            return await Group.updateGroup(args.input, context);
        },
        addUserToGroup: async (obj, {username, groupname}, context) => {
            await User.addUserToGroup(username, groupname, context);
            return await Group.addUserToGroup(username, groupname, context);
        },
        deleteGroup: async (obj, {groupname}, context) => {
            return await Group.deleteGroup(groupname, context);
        },
        addCategory: async (obj, {category, group}, context) => {
            return await Group.addCategory(category, group, context);
        },
        deleteCategory: async (obj, {category, group}, context) => {
            return await Group.deleteCategory(category, group, context);
        },
        addModerator: async (obj, {groupname, username}, context) => {
            await Group.addModeratorToGroup(username, groupname, context);
            await User.addModeratorStatus(username, groupname, context);
            return 'Success';

        }
    },
    Group: {
        members: async (group, _, context) => {
            return await User.getUsersById(group.members, context);
        },
        moderators: async (group, _, context) => {
            return await User.getUsersById(group.moderators, context);
        }
    }
};

export default () => GroupResolvers;