/**
 * Created by Lukas on 07-Apr-17.
 */

import Item from '../ItemType/itemTypeSchema';

const Tag = `
    type Tag {
        id: String!
        name: String
        items: [Item]
        }
`;

export default () => [Tag];