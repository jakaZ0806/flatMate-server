/**
 * Created by Lukas on 07-Apr-17.
 */

import itemType from '../ItemType/itemTypeSchema';

const recipeItem = `
    type RecipeItem {
        item: [ItemType]
        amount: Int
`;

export default () => [recipeItem, itemType];