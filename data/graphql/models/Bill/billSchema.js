/**
 * Created by Luki on 31.03.17.
 */
import User from '../User/userSchema';
import Group from '../Group/groupSchema';
import Item from '../Item/itemSchema';

const Bill = `
    type Bill {
        id: String
        sum: Float
        items: [Item]
        group: Group
        owner: User
        paidBy: User
        consumers: [User]
        date: String
        name: String
    }
    input BillInput {
        items: [ItemInput]
        group: String
        paidBy: String
        date: String
        name: String
    }
`;
export default () => [Bill]