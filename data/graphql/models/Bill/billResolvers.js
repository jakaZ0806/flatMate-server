/**
 * Created by Lukas on 27-Apr-17.
 */
import * as Bill from '../../../businessLogic/repositories/Bill'
import * as User from '../../../businessLogic/repositories/User'
import * as Item from '../../../businessLogic/repositories/Item'

const BillResolvers = {
    Query: {
        bill: async (obj, {id}, context) => {
            if (id) {
                return await Bill.getBillById(id, context);
            }
        },
        bills: async (obj, {owner, paidBy, group, fromDate, toDate, consumer}, context) => {
            if (owner) {
                return await Bill.getBillsByOwner(owner, context);
            }
        },
        billsByGroup: async (obj, {groupname, from, to}, context) => {
            if (!from) {
                from = new Date(0);
            }
            if (!to) {
                to = new Date(Date.now());
            }
            return await Bill.getBillsByGroupAndTime(groupname, from, to, context);
        }
        },
    Mutation: {
        newBill: async (obj, {input }, context) => {
            return await Bill.newBill(input, context);
        },
        deleteBill: async (obj, {id}, context) => {
            return await Bill.deleteBill(id, context);
        }
    },
    Bill : {
        date: async(bill, _, context) => {
            return new Date(bill.date).toISOString();
        },
        consumers: async(bill, _, context) => {
            return await User.getUsersById(bill.consumers, context);
        },
        paidBy: async(bill, _, context) => {
            return await User.getUserById(bill.paidBy, context);
        },
        items: async(bill, _, context) => {
            return await Item.getItemsById(bill.items, context);
        }
    }
    };

export default () => BillResolvers;