/**
 * Created by Lukas on 07-Apr-17.
 */
const AuthResponse = `
    type AuthResponse {
        success: Boolean!
        error: String
        token: String
        userActions: UserActions
    }
`;

export default () => [AuthResponse];