/**
 * Created by Lukas on 07-Apr-17.
 */
const AuthError = `
type AuthError {
    key: String,
    message: String!
}
`;

export default () => [AuthError];