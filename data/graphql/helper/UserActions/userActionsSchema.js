const UserActions = `
type UserActions {
    changePassword: Boolean
}
`;

export default () => [UserActions];