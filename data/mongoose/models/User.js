/**
 * Created by Lukas on 14-Dec-16.
 */
import mongoose from 'mongoose';
import validator from 'mongoose-unique-validator';
import Bill from './Bill';
import Item from './Item';
import Group from './Group'


const userSchema = mongoose.Schema({
    username: {type: String, unique: true},
    email: String,
    password: String,
    admin: Boolean,
    firstName: String,
    lastName: String,
    groups: [{ type : mongoose.Schema.Types.ObjectId, ref: 'Group'}],
    changePassword: Boolean,
    moderator: [{ type : mongoose.Schema.Types.ObjectId, ref: 'Group'}],
    superUser: Boolean
});

userSchema.plugin(validator);


//Cleanup
userSchema.post('findOneAndRemove', function(result){
    Bill.update({}, {$pull: { consumers: result._id }}, {multi: true}).exec();
    Item.update({}, {$pull: { consumers: result._id }}, {multi: true}).exec();
    Bill.update({paidBy: result._id}, {paidBy: null}).exec();
    Bill.update({owner: result._id}, {owner: null}).exec();
    Item.update({paidBy: result._id}, {paidBy: null}).exec();
    Item.update({owner: result._id}, {owner: null}).exec();
    Group.update({}, {$pull: { members: result._id}}, {multi: true}).exec();
    Group.update({}, {$pull: { moderators: result._id}}, {multi: true}).exec();
});


export default mongoose.model('User', userSchema);