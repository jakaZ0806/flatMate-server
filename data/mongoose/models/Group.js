/**
 * Created by Lukas on 28-Apr-17.
 */
import mongoose from 'mongoose';
import Bill from './Bill';
import Item from './Item';
import User from './User';
import validator from 'mongoose-unique-validator';

const groupSchema = mongoose.Schema({
    name: {type: String, unique: true},
    description: String,
    members: [{ type : mongoose.Schema.Types.ObjectId, ref: 'User'}],
    categories: [{type: String, unique: false}],
    moderators: [{type : mongoose.Schema.Types.ObjectId, ref: 'User'}],
    archived: Boolean
});

groupSchema.plugin(validator);

groupSchema.post('findOneAndRemove', function(result){
    Bill.update({group: result._id}, {group: null }, {multi: true}).exec();
    Item.update({group: result._id}, { group: null}, {multi: true}).exec();
    User.update({}, {$pull: { groups: result._id }}, {multi: true}).exec();
    User.update({}, {$pull: { moderator: result._id }}, {multi: true}).exec();


});

export default mongoose.model('Group', groupSchema);