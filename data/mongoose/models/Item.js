/**
 * Created by Lukas on 28-Apr-17.
 */
import mongoose from 'mongoose';
import Bill from './Bill';


const itemSchema = mongoose.Schema({
    name: String,
    type: { type : mongoose.Schema.Types.ObjectId, ref: 'ItemType'},
    price: Number,
    amount: Number,
    unitSize: Number,
    consumers: [{ type : mongoose.Schema.Types.ObjectId, ref: 'User'}],
    paidBy: { type : mongoose.Schema.Types.ObjectId, ref: 'User'},
    date: Date,
    group: { type : mongoose.Schema.Types.ObjectId, ref: 'Group'},
    owner: { type : mongoose.Schema.Types.ObjectId, ref: 'User'},
    category: String
});

itemSchema.post('findOneAndRemove', function(result){
    Bill.update({}, {$pull: { items: result._id }}, {multi: true}).exec();
});


export default mongoose.model('Item', itemSchema);