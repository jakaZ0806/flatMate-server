/**
 * Created by Lukas on 28-Apr-17.
 */
import mongoose from 'mongoose';

const billSchema = mongoose.Schema({
    sum: Number,
    name: String,
    items: [{ type : mongoose.Schema.Types.ObjectId, ref: 'Item'}],
    group: { type : mongoose.Schema.Types.ObjectId, ref: 'Group'},
    owner: { type : mongoose.Schema.Types.ObjectId, ref: 'User'},
    paidBy: { type : mongoose.Schema.Types.ObjectId, ref: 'User'},
    consumers: [{ type : mongoose.Schema.Types.ObjectId, ref: 'User'}],
    date: Date
});

export default mongoose.model('Bill', billSchema);


//     items: [{ type : mongoose.Schema.Types.ObjectId, ref: 'Item'}],
//consumers: [{ type : mongoose.Schema.Types.ObjectId, ref: 'User'}],