/**
 * Created by Lukas on 28-Apr-17.
 */
import * as User from '../repositories/User'
import * as Item from '../repositories/Item'
import * as budgetCalculator from '../budgetCalculator';

import Group from '../../mongoose/models/Group';

const checkUserLogin = function (user) {
    if (!user) {
        throw errorObj({
            _error: 'Unauthorized.'
        });
    }
};

//Error Object to prettify Error Messages on Client
const errorObj = obj => {
    return new Error(JSON.stringify(obj));
};

async function newGroup(input, context) {
    checkUserLogin(context.user);
    if (context.user.admin || context.user.superUser) {
        const memberObj = await User.getUserByUsername(input.initialMember, context);
        const user = await User.getUserByUsername(context.user.username, context);
        let member;
        if (memberObj !== null) {
            member = memberObj;
        } else {
            member = user;
        }
        
        const newGroup = await new Group({
            name: input.name,
            description: input.description,
            members: [member._id],
            moderators: [member._id],
            archived: false
        });

        // save the Group
        let group;
        try {
            group = await newGroup.save();
        } catch (e) {
            //only handling duplicate name errors for now
            throw errorObj({
                _error: e
            });
        }
        await User.addUserToGroup(member.username, group.name, context);
        await User.addModeratorStatus(member.username, group.name, context);
        return group;

    } else {
        throw errorObj({
            _error: 'Need SuperUser or Admin Privileges.'
        });
    }

}

async function updateGroup(input, context) {
    checkUserLogin(context.user);
    const user = await User.getUserByUsername(context.user.username, context);
    const groupObj = await getGroupByName(input.name, context);
    if (user.moderator.some(document => document.equals(groupObj._id)) || user.admin) {
        return await Group.findOneAndUpdate({
            name: input.name
        }, {
            $set: input
        }, {
            new: true
        });

    } else {
        throw errorObj({
            _error: 'Unauthorized.'
        });
    }
}

async function getGroupByName(groupname, context) {
    checkUserLogin(context.user);
    return await Group.findOne({
        'name': groupname
    });

}

function getGroupById(groupId, context) {
    checkUserLogin(context.user);
    return new Promise(resolve => {
        const group = Group.findOne({
            '_id': groupId
        }, function (err, result) {
            if (err) return handleError(err);
            return result;
        });
        resolve(group);
    })
}

async function getGroupsById(ids, context) {
    checkUserLogin(context.user);
    return await Group.find()
        .where('_id')
        .in(ids)
        .exec(function (err, records) {
            return records;
        });
}

async function getAllGroups(context) {
    return await Group.find({"archived": false});
}

async function getManagedGroups(username, context) {
    checkUserLogin(context.user);
    if (context.user.admin || context.user.superUser) {
        return await Group.find({"archived": false});
    } else {
        return await getGroupsForUser(username, context);
    }
}

async function getGroupsForUser(username, context) {
    checkUserLogin(context.user);
    const user = await User.getUserByUsername(context.user.username, context);
    return await Group.find({"archived": false, "members": {"$in": [user._id]}});
}

async function addUserToGroup(username, groupname, context) {
    checkUserLogin(context.user);
    const userObj = await User.getUserByUsername(username, context);
    const groupObj = await getGroupByName(groupname, context);
    const user = await User.getUserByUsername(context.user.username, context);
    if (user.moderator.some(document => document.equals(groupObj._id)) || context.user.admin) {
        await Group.findOneAndUpdate({
            'name': groupname
        }, {
            "$addToSet": {
                "members": userObj._id
            }
        }, {
            new: true
        });
    } else {
        throw errorObj({
            _error: 'Unauthorized. Moderator Status needed.'
        });
    }
    return userObj;
}

async function deleteGroup(groupname, context) {
    checkUserLogin(context.user);
    const user = await User.getUserByUsername(context.user.username, context);
    const groupObj = await getGroupByName(groupname, context);
    if (user.moderator.some(document => document.equals(groupObj._id)) || context.user.admin) {
        return await Group.findOneAndUpdate({
            'name': groupname
        }, {archived: true});
    } else {
        throw errorObj({
            _error: 'Unauthorized. Admin Privileges needed.'
        });

    }
}

async function addCategory(category, groupname, context) {
    checkUserLogin(context.user);
    const user = await User.getUserByUsername(context.user.username, context);
    const groupObj = await getGroupByName(groupname, context);
    if (user.moderator.some(document => document.equals(groupObj._id)) || context.user.admin) {
        await Group.findOneAndUpdate({
            'name': groupname
        }, {
            "$addToSet": {
                "categories": category
            }
        }, {
            new: true
        });
    } else {
        throw errorObj({
            _error: 'Unauthorized. Admin Privileges needed.'
        });

    }
    return category;
}

async function deleteCategory(category, group, context) {
    checkUserLogin(context.user);
    const user = await User.getUserByUsername(context.user.username, context);
    const groupObj = await getGroupByName(groupname, context);
    if (user.moderator.some(document => document.equals(groupObj._id)) || context.user.admin) {
        return await Group.update({
            'name': group
        }, {
            $pull: {
                categories: category
            }
        }, {
            multi: true
        }).exec();
    } else {
        throw errorObj({
            _error: 'Unauthorized. Admin Privileges needed.'
        });

    }
}

async function getGroupBudget(groupname, context) {
    checkUserLogin(context.user);
    const group = await getGroupByName(groupname, context);
    const users = await User.getUsersById(group.members, context);
    const user = await User.getUserByUsername(context.user.username, context);
    if (user.groups.some(document => document.equals(group._id)) || context.user.admin) {
        const ownBudget = await budgetCalculator.getBudgetForUser(user, group);
        const userBudgets = [];
        for (let i in users) {
            if (users[i]._id.toString() !== context.user._id.toString()) {
                const budget = await budgetCalculator.getBudgetForUser(users[i], group);
                await userBudgets.push({
                    "group": group,
                    "user": users[i],
                    "budget": budget
                });
            }
        }
        return await {
            group: group,
            budgets: userBudgets,
            ownBudget: {
                budget: ownBudget,
                user: user
            }
        };
    } else {
        throw errorObj({
            _error: 'Unauthorized. Admin Privileges needed or need to be in group.'
        });

    }
}

async function getCategorySummary(groupname, from, to, context) {
    checkUserLogin(context.user);
    const group = await getGroupByName(groupname, context);
    const items = await Item.getItemsForGroupAndDate(groupname, from, to, context);

    let result = [];

    for (let i = 0; i < group.categories.length; ++i) {
        result.push({
            name: group.categories[i],
            value: 0
        });
    }

    for (let i = 0; i < items.length; ++i) {
        for (let j = 0; j < group.categories.length; ++j) {
            if (items[i].category === group.categories[j]) {
                result[j].value += items[i].price;
            }
        }
    }
    return result;

}

async function addModeratorToGroup(username, groupname, context) {
    checkUserLogin(context.user);
    const userObj = await User.getUserByUsername(username, context);
    const groupObj = await getGroupByName(groupname, context);
    const user = await User.getUserByUsername(context.user.username, context);
    if (user.moderator.some(document => document.equals(groupObj._id)) || context.user.admin) {
        return await Group.findOneAndUpdate({
            'name': groupname
        }, {
            "$addToSet": {
                "moderators": userObj._id
            }
        }, {
            new: true
        });
    } else {
        throw errorObj({
            _error: 'Unauthorized. Moderator Status needed.'
        });
    }


}

export {
    newGroup,
    updateGroup,
    getGroupByName,
    getManagedGroups,
    getGroupById,
    getGroupsById,
    getAllGroups,
    addUserToGroup,
    deleteGroup,
    addCategory,
    deleteCategory,
    getGroupBudget,
    getCategorySummary,
    addModeratorToGroup
}