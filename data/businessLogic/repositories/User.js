/**
 * Created by Lukas on 16-Nov-16.
 */

import User from '../../mongoose/models/User';
import * as Group from '../repositories/Group';
import * as budgetCalculator from '../budgetCalculator';
import bcrypt from 'bcrypt';

const checkUserLogin = function (user) {
    if (!user) {
        throw errorObj({
            _error: 'Unauthorized.'
        });
    }
};

//Error Object to prettify Error Messages on Client
const errorObj = obj => {
    return new Error(JSON.stringify(obj));
};

//Adds a User to the Database
async function addUser(input, context) {
    let hash = bcrypt.hashSync(input.password, 10);
    const newUser = new User({
        firstName: input.firstName,
        lastName: input.lastName,
        password: hash,
        username: input.username,
        admin: false,
        email: input.email,
        groups: [],
        changePassword: false,
        superUser: false,
        moderator: []
    });

    let user;
    try {
        user = await newUser.save();
            
    } catch (e) {
        //only handling duplicate username errors for now
        throw errorObj({
            _error: e.errors.username.message
        });
    }
    return user;
}

//Delete User By Username
async function deleteUser(username, context) {
    checkUserLogin(context.user);
    if (context.user.admin) {
        return await User.findOneAndRemove({
            'username': username
        });

    } else {
        throw errorObj({
            _error: 'Unauthorized. Admin Privileges needed.'
        });

    }
}

//Gets all Users
function getAllUsers(context) {
    checkUserLogin(context.user);
    return User.find({});
}

async function getUserByUsername(username, context) {
    return await User.findOne({
        'username': username
    });
}


//Get a User By Id
async function getUserById(id, context) {

    return await User.findOne({
        '_id': id
    });
}

//Get an Array of Users by Ids
async function getUsersById(ids, context) {

    return await User.find()
        .where('_id')
        .in(ids)
        .exec(function (err, records) {
            return records;
        });
}

async function updateUser(args, context) {
    checkUserLogin(context.user);
    if ((args.username === context.user.username) || (context.user.admin)) {
        return await User.findOneAndUpdate({
            username: args.username
        }, {
            $set: args.input
        }, {
            new: true
        });

    } else {
        throw errorObj({
            _error: 'Unauthorized.'
        });
    }
}

async function changeUserPassword(username, args, context) {
    checkUserLogin(context.user);
    return await User.findOneAndUpdate({
        username: username
    }, {
        $set: args
    }, {
        new: true
    });
}


async function addUserToGroup(username, groupname, context) {
    checkUserLogin(context.user);
    const groupObj = await Group.getGroupByName(groupname, context);
    return await User.findOneAndUpdate({
        'username': username
    }, {
        "$addToSet": {
            "groups": groupObj._id
        }
    }, {
        new: true
    });
}

async function getAllBudgets(username, context) {
    checkUserLogin(context.user);
    const user = await getUserByUsername(username, context);
    let result = [];

    for (let i = 0; i < user.groups.length; ++i) {
        const group = await Group.getGroupById(user.groups[i], context);
        if (!group.archived) {
        const groupBudget = budgetCalculator.getBudgetForUser(user, group);
        result.push({
            "group": group,
            "user": user,
            "budget": groupBudget
        });
    }
    }

    return result;
}

async function getUserSummary(username, context) {
    checkUserLogin(context.user);
    const user = await getUserByUsername(username, context);
    let groupBudgets = [];
    const userBudgets = await getAllBudgets(username, context);
    for (let i = 0; i < user.groups.length; ++i) {
        const group = await Group.getGroupById(user.groups[i], context);
        if (!group.archived) {
        groupBudgets.push(await Group.getGroupBudget(group.name, context));
        }
    }
    return {
        user: user,
        groupBudgets: groupBudgets,
        userBudgets: userBudgets
    };
}

async function addModeratorStatus(username, groupname, context) {
    checkUserLogin(context.user);
    const user = await getUserByUsername(context.user.username, context);
    const groupObj = await Group.getGroupByName(groupname, context);
    if (user.admin || user.superUser || user.moderator.some(document => document.equals(groupObj._id))) {
        const groupObj = await Group.getGroupByName(groupname, context);
        return await User.findOneAndUpdate({
            'username': username
        }, {
            "$addToSet": {
                "moderator": groupObj._id
            }
        }, {
            new: true
        });
    } else {
        throw errorObj({
            _error: 'Unauthorized. Moderator Status needed.'
        });
    }

}

export {
    getAllUsers,
    addUser,
    getUserByUsername,
    deleteUser,
    getUserById,
    getUsersById,
    updateUser,
    addUserToGroup,
    changeUserPassword,
    getAllBudgets,
    getUserSummary,
    addModeratorStatus
}