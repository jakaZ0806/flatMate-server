/**
 * Created by Lukas on 28-Apr-17.
 */
import Item from '../../mongoose/models/Item';
import * as budgetCalculator from '../budgetCalculator';
import * as User from '../repositories/User'
import * as Group from '../repositories/Group'

import Bill from '../../mongoose/models/Bill';

const checkUserLogin = function(user) {
    if (!user) {
        throw errorObj({_error: 'Unauthorized.'});
    }
};

//Error Object to prettify Error Messages on Client
const errorObj = obj => {
    return new Error(JSON.stringify(obj));
};

async function getItemById(id, context) {
    return await Item.findOne({'_id': id});
}

async function getItemsById(ids, context) {
    return await Item.find()
        .where('_id')
        .in(ids)
        .exec(function (err, records) {
            return records;
        });
}

async function newItem(input, context) {
    const consumers = await budgetCalculator.calcConsumers([input.item]);
    const payer = await User.getUserByUsername(input.paidBy, context);
    const group = await Group.getGroupByName(input.group, context);

        const newItem = new Item({
            name: input.item.name,
            price: input.item.price,
            amount: input.item.amount,
            unitsize: input.item.unitsize,
            consumers: consumers,
            date: input.date,
            paidBy: payer._id,
            group: group._id,
            owner: context.user._id,
            category: input.item.category
        });


        // save the Item
    try {
        return await newItem.save();
    }
    catch(e) {
        throw errorObj({_error: e.errors});
    }

}

async function deleteItemById(itemId, context) {
    if (context.user.admin) {
        return await Item.findOneAndRemove({'_id': itemId});

    }
    else {
        throw errorObj({_error: 'Unauthorized. Admin Privileges needed.'});

    }
}

async function deleteItemsById(itemIds, context) {
    if (context.user.admin) {
        return await Item.findOneAndRemove({_id: {$in: itemIds}});

    }
    else {
        throw errorObj({_error: 'Unauthorized. Admin Privileges needed.'});

    }
}

async function getItemsForGroupAndDate(groupname, from, to, context) {
    const group = await Group.getGroupByName(groupname, context);
    return await Item.find({'group': group._id, date: {"$gte": new Date(from), "$lt": new Date(to)}})
}

export {newItem, deleteItemById, deleteItemsById, getItemById, getItemsById, getItemsForGroupAndDate}