/**
 * Created by Lukas on 28-Apr-17.
 */

import * as budgetCalculator from '../budgetCalculator';
import * as Item from '../repositories/Item';
import * as User from '../repositories/User';
import * as Group from '../repositories/Group'
import Bill from '../../mongoose/models/Bill';

const checkUserLogin = function(user) {
    if (!user) {
        throw errorObj({_error: 'Unauthorized.'});
    }
};

//Error Object to prettify Error Messages on Client
const errorObj = obj => {
    return new Error(JSON.stringify(obj));
};

async function newBill(input, context) {
    const total = budgetCalculator.calcSum(input.items);
    let consumers = await budgetCalculator.calcConsumers(input.items, context);
    let paidByObj = await User.getUserByUsername(input.paidBy, context);
    const  groupObj = await Group.getGroupByName(input.group, context);
    let itemIds = [];
    for  (const i in input.items) {
        itemIds.push(await Item.newItem({item: input.items[i], date: input.date, paidBy: input.paidBy, group: input.group}, context));
    }

    const newBill = new Bill({
        items: itemIds,
        group: groupObj._id,
        owner: context.user._id,
        paidBy: paidByObj._id,
        sum: total,
        consumers: consumers,
        name: input.name,
        date: input.date

    });
    // Save the Bill
    try {
        return await newBill.save();

    }
    catch(e) {
        throw errorObj({_error: e});
    }


}

async function getBillById(id, context) {
    return await Bill.findOne({'_id': id});
}

async function getBillsByGroupAndTime(groupname, from, to, context) {
    const  groupObj = await Group.getGroupByName(groupname, context);
    return await Bill.find({group: groupObj._id, date: {"$gte": new Date(from), "$lt": new Date(to)}})
}

async function deleteBill(billId, context) {
    checkUserLogin(context.user);
    const  billObj = await getBillById(billId, context);
    const owner = await User.getUserById(billObj.owner);
    const user = await User.getUserByUsername(context.user.username);
    if (context.user.admin || context.user.username === owner.username || user.moderator.some(document => document.equals(billObj.group))) {
        await Item.deleteItemsById(billObj.items, context);
        return await Bill.findOneAndRemove({'_id': billId});

    }
    else {
        throw errorObj({_error: 'Unauthorized. You can only delete your own Bills.'});

    }
}

//TODO:
function getBillsByOwner(owner, context) {
    return Bill.find({owner: owner})
}

export {newBill, getBillsByOwner, deleteBill, getBillById, getBillsByGroupAndTime};