/**
 * Created by Lukas on 28-Apr-17.
 */

import Item from '../mongoose/models/Item';
import * as UserBL from './repositories/User'

function calcSum(items) {
    let sum = 0;
    for (let i in items) {
            sum += items[i].price;
        }
    return sum;n
}

async function calcConsumers(items, context) {
    //console.log(items);
    let consumers = new Set();
    for (let i in items) {
        for (let j in items[i].consumers) {
            const consumer = await UserBL.getUserByUsername(items[i].consumers[j], context);
            if(consumers.has(consumer._id) === false) {
                consumers.add(consumer._id.toString());
                }
            }
        }
    return await [...consumers];

}
async function getPaidSumForUser(user, group, from, to) {
    let sum = 0;
    await Item.find({paidBy: user._id, group: group._id}, function (err, records) {
       for (let i in records) {
          sum +=  records[i].price;
       }
       return sum;
    });
    return await sum;
}

async function getConsumedSumForUser(user, group, from, to) {
    let sum = 0;
    await Item.find()
        .where('group').equals(group._id)
        .where({ consumers: { "$in" : [user._id]} })
        .exec(function (err, records) {

            for (let i in records) {
                sum +=  records[i].price / records[i].consumers.length;
            }
        });
    return await sum;
}

async function getBudgetForUser(user, group) {
    const paid = await getPaidSumForUser(user, group, 0, Date.now());
    const consumed = await getConsumedSumForUser(user, group, 0, Date.now());
    return await Math.round((paid - consumed) * 100) / 100;
}


export {calcSum, calcConsumers, getPaidSumForUser, getConsumedSumForUser, getBudgetForUser}