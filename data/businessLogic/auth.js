/**
 * Created by Lukas on 29-Apr-17.
 */
import * as User from './repositories/User'
import * as config from '../config.json';
import jwt from 'jsonwebtoken';
import bcrypt from 'bcrypt';

//Error Object to prettify Error Messages on Client
const errorObj = obj => {
    return new Error(JSON.stringify(obj));
};


const checkUserLogin = function(user) {
    if (!user) {
        throw errorObj({_error: 'Unauthorized.'});
    }
};

async function login(username, password, context) {
    //create response object
    let response =  {
        success: false,
        error: '',
        token: '',
        userActions: {}                    
        };
    // find the user
    const user = await User.getUserByUsername(username, context);
        if (!user) {
            response = {
                success: false,
                error: 'User not found!'
            };
        } else if (user) {


            const res = await bcrypt.compare(password, user.password);
                if(res) {
                 // Passwords match
                 delete(user.password);
                 // create a token
                 const token = jwt.sign(user.toObject(), config.jwtSecret, {
                     expiresIn: "7 days"
                 });


                 // return the information including token as JSON
                response.success = true;
                response.token = token;
                if (user.changePassword === true) {
                    response.userActions.changePassword = true;
                }
                } else {
                 // Passwords don't match
                    response = {
                        success: false,
                        error: 'Wrong Password!'
                    }
                }
        }
    return response;
    
}

async function changePassword(username, currentPassword, newPassword, context) {
    const user = await User.getUserByUsername(username, context);
    if (!user.admin && user.username === username) {
        const res = await bcrypt.compare(currentPassword, user.password);
        if(res) {
        bcrypt.hash(newPassword, 10, function(err, hash) {
            User.updateUser({username: username, input: {password: hash, changePassword: false}}, context);
          });
        } else {
            return await 'Wrong Password';
        }
    } else if (user.admin) {
        bcrypt.hash(newPassword, 10, function(err, hash) {
            User.updateUser({username: username, input: {password: hash, changePassword: false}}, context);
          });
    }
    return await 'Success';
}

export {login, changePassword};