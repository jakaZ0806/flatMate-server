/**
 * Created by Lukas on 15-Nov-16.
 */
import express from 'express';

import { graphqlExpress, graphiqlExpress } from 'graphql-server-express';
import bodyParser from 'body-parser';

import { createServer } from 'http';
import expressjwt from 'express-jwt';

import mongoose from 'mongoose';
import User from './data/mongoose/models/User'

import schema from './data/graphql/schema';
import * as config from './data/config.json';
import bcrypt from 'bcrypt';

const allowCrossDomain = function(req, res, next) {
    res.header('Access-Control-Allow-Origin', '*');
    res.header('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE');
    res.header('Access-Control-Allow-Headers', 'Content-Type');

    next();
};

const graphQLServer = express();
mongoose.Promise = require('bluebird');
mongoose.connect(config.dbConnection, {useMongoClient: true});

mongoose.connection.on('connected', function () {
    mongoose.connection.db.collection('users').count(function (err, count) {
        if (count == 0) {
            console.log("No User-Collection found in Database. Creating Initial admin-User. DELETE after Setup!!!");
            let hash = bcrypt.hashSync('initial', 10);
            const newUser = new User({
                firstName: 'Mr.',
                lastName: 'Admin',
                password: hash,
                username: 'admin',
                admin: true,
                email: 'nomail',
                groups: [],
                changePassword: true,
                superUser: true,
                moderator: []
            });

            // save the sample user
            newUser.save(function (err) {
                if (err) throw err;
            });
        }
    });
});



    graphQLServer.use(bodyParser.urlencoded({ extended: true }));
graphQLServer.use(allowCrossDomain);

//Use JWT for Authentication

graphQLServer.use(expressjwt({
    secret: config.jwtSecret,
    credentialsRequired: false
}));

graphQLServer.use('/graphql', bodyParser.json(), graphqlExpress((req) => ({
    schema,
    context: {user: req.user}
})));

graphQLServer.use('/graphiql', graphiqlExpress({
    endpointURL: '/graphql',
}));


//START GraphQL Server
graphQLServer.listen(config.graphQLPort, () => console.log(
    `GraphQL Server is now running on http://localhost:${config.graphQLPort}/graphql`
));

